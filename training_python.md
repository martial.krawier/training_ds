# Coding Session 1

## Pré requis

* python (ceinture jaune) : utilisation des types de base, écriture de fonctions simples ...
* pandas, numpy : commandes de bases
* connaissance correspondance SQL et pandas

#### Bibliothèque

* https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/comparison_with_sql.html
* https://pandas.pydata.org/pandas-docs/stable/getting_started/10min.html


## 1 RDB : Blob Storage : Storage dans le cloud Azure

### Présentation des fichiers

### Lecture des fichiers depuis le blob via pandas dans le lab

### Connection au DWH 

### Requêtes dans le DWH depuis le lab 

* Connection
* Quelques exemples

## 2 Pandas et SQL  Pros and Cons : discussion et exemples de code 

###  +++ Pandas
* une place pour tout faire (pas de sql to excel et vice versa
* application de fonctions à des tables : implique de sortir du sql et de revenir
    ** on peut le faire via hive dans une table sql
* travail avec des sources de nature différentes 
* plus orientées programmation
* plus statique :  DDL et DML  en SQL, la création des tables est plus simple en python

### +++ SQL

* rapidité des traitements (dépend du serveur, de la db ....)
* requêtes sql optimisées sont rapides (mais pas faciles à faire)

Mais pour du passage à l'échelle très important, on va devoir utiliser des outils plus Big Data tels que spark ou h2O.  On pourra faire du python ou du SQL 

SQL Server ou Teradata permettent de faire appel à  R ou python 

## 3 Traitement des données externes

### Météo

## 4 Parcours Client

### Présentation Evenements et Visites 

## 5 Workflows de travail : discussion

* D'abord faire les traitements en SQL sur le DWH
* Importer les données du DWH au datalab 
* Puis traitement Pandas avec
   * croisements données externes
   * données autres bases telles que mongo

## 6 A définir Travaux Pratiques sur cas concret celio CRM

Ex: récupération les données de tickets croisées avec les clients et méteo pour des tableaux de bord ?

....
* L'équipe CRM formalise le problème
* Support DSI pour indication des librairies, fonctions à utiliser ...
