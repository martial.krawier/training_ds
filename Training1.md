# Préparation Environnements Celio


## Présentation des outils 


### Rstudio Server

http://10.6.32.4:8787/ puis user/password


### Jupyter 

http://10.6.32.4:8888


#### JupyterHub 

* Version Tree

http://10.6.32.4:8888/user/user/tree

* Version Lab
http://10.6.32.4:8888/user/user/lab

Le mode lab est le mode conseillé.

* Faire le tour des fonctionalités de jupyter


### Accès aux blob storage Azure

### Rshiny Server




## Arborescence Serveur

####  /home/user

* Vos fichiers et installs locales


### Création Clé ssh (par machine)

Dans un terminal :

``` bash

cd 
mkdir .ssh
cd .ssh
ssh-keygen -t rsa -b 2048 -C "presta_mkrawier"
name key accordingly to VM
add passphrase  (like a password you don't see )
```

Vous avez creée une clé privée et une clé publique. 
La clé publique est partageable,pas la clé privée


#### /home/data

```
drwxrwxr-x  3 datauser        data            4096 Apr 16 14:33 ./
drwxr-xr-x 15 root            root            4096 Apr 14 08:34 ../
drwxrwxr-x  3 presta_mkrawier presta_mkrawier 4096 Apr 16 14:36 eulerian/
drwxrwxr-x  3 presta_mkrawier presta_mkrawier 4096 Apr 16 14:36 db/
drwxrwxr-x  3 presta_mkrawier presta_mkrawier 4096 Apr 16 14:36 blob

```


Admin : Ajouter les users aux groupes unix data et/ou docker


## Python 

### Introduction Virtual env 

https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html

https://towardsdatascience.com/a-guide-to-conda-environments-bc6180fc533



``` bash
 /opt/miniconda3/bin/conda init
 conda create -n crm1 python=3.7 -y
 conda activate crm1
 
 #pip install -r requirements.txt /
 
 
conda install -c anaconda ipykernel -y
python -m ipykernel install --user --name=crm1
```

Vous pouvez à présent utiliser crm1 dans le jupyter lab


### Lien Jupyter et Virtual env

### Connection SQL 

### Connection Mongo DB


## Training Git

### Set up

* Création compte bitbucket/gitlab ?
* Création des clés ssh par les users et ajout à BitBucket




### Documents

* https://nceas.github.io/training-git-intro/getting-started-with-git-rstudio.html 



## Blob Storage

https://docs.microsoft.com/fr-fr/azure/storage/

```
wget https://http://packages.microsoft.com/config/ubuntu/19.04/packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
sudo apt-get update
sudo apt-get install blobfuse

sudo mkdir /mnt/ramdisk
sudo mount -t tmpfs -o size=16g tmpfs /mnt/ramdisk
sudo mkdir /mnt/ramdisk/blobfusetmp
sudo chown user:user  /mnt/ramdisk/blobfusetmp
sudo chown datauser:data  /mnt/ramdisk/blobfusetmp

sudo mkdir /mnt/resource/blobfusetmp -p
sudo chown datauser:data  /mnt/resource/blobfusetmp

----------------------------

accountName devrdbceliostd
accountKey YOUR KEY
containerName ref-crm
-----------------------------


chmod 600 fuse_connection.cfg
mkdir /home/data/blob
sudo blobfuse /home/data/blob  --tmp-path=/mnt/resource/blobfusetmp  --config-file=./fuse_connection.cfg -o attr_timeout=240 -o entry_timeout=240 -o negative_timeout=120 -o allow_other
```



### Avancé

#### Création d'un module

#### Utilisation pycharm